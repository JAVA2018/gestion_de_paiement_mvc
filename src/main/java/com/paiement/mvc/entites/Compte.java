package com.paiement.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Compte implements Serializable{
	
	@Id
	@GeneratedValue
	private int idCompte;
	
	private BigDecimal budgetFonctionnel;
	private BigDecimal budgetInvestissement;
	
	@ManyToOne
	@JoinColumn(name = "idEt")
	private Etablissement etablissement;
	
	@OneToMany(mappedBy = "compte")
	private List<FluxEntrant> fluxEntrants;
	
	@OneToMany(mappedBy = "compte")
	private List<Creditouvert> creditouverts;
	@OneToMany(mappedBy = "compte")
	private List<ModePaiement> modePaiements;

	public Etablissement getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}

	public int getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}

	public BigDecimal getBudgetFonctionnel() {
		return budgetFonctionnel;
	}

	public void setBudgetFonctionnel(BigDecimal budgetFonctionnel) {
		this.budgetFonctionnel = budgetFonctionnel;
	}

	public BigDecimal getBudgetInvestissement() {
		return budgetInvestissement;
	}

	public void setBudgetInvestissement(BigDecimal budgetInvestissement) {
		this.budgetInvestissement = budgetInvestissement;
	}

	public List<FluxEntrant> getFluxEntrants() {
		return fluxEntrants;
	}

	public void setFluxEntrants(List<FluxEntrant> fluxEntrants) {
		this.fluxEntrants = fluxEntrants;
	}

	public List<Creditouvert> getCreditouverts() {
		return creditouverts;
	}

	public void setCreditouverts(List<Creditouvert> creditouverts) {
		this.creditouverts = creditouverts;
	}
	
	

}
