package com.paiement.mvc.entites;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class ModePaiement implements Serializable {
	@Id
	@GeneratedValue
	private int idModePaiement;
	
	private String type;
	 private double montant;
	private Date date;
	@ManyToOne
	@JoinColumn(name = "idCompte")
	private Compte compte;
	@ManyToOne
	@JoinColumn(name = "idBenificaire")
	private Benificiaire benificiaire;
	@ManyToOne
	@JoinColumn(name = "idExercice")
	private Exercice exercice;
	@OneToMany(mappedBy = "modepaiement")
	private List<OrderPaiement> orderpaiements;
	

	public int getIdModePaiement() {
		return idModePaiement;
	}

	public void setIdModePaiement(int idModePaiement) {
		this.idModePaiement = idModePaiement;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	public Exercice getExercice() {
		return exercice;
	}

	public void setExercice(Exercice exercice) {
		this.exercice = exercice;
	}

	public List<OrderPaiement> getOrderpaiements() {
		return orderpaiements;
	}

	public void setOrderpaiements(List<OrderPaiement> orderpaiements) {
		this.orderpaiements = orderpaiements;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Benificiaire getBenificiaire() {
		return benificiaire;
	}

	public void setBenificiaire(Benificiaire benificiaire) {
		this.benificiaire = benificiaire;
	}
	
	

}
