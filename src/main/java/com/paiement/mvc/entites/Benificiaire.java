package com.paiement.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Benificiaire implements Serializable{
	
	@Id
	@GeneratedValue
	private int idBenificaire;
	
	@NotEmpty
	private String nom;
	
	@NotEmpty
	private String prenom;
	@OneToMany(mappedBy = " benificiaire")
	private List<ModePaiement> modePaiements;

	public int getIdBenificaire() {
		return idBenificaire;
	}

	public void setIdBenificaire(int idBenificaire) {
		this.idBenificaire = idBenificaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<ModePaiement> getModePaiements() {
		return modePaiements;
	}

	public void setModePaiements(List<ModePaiement> modePaiements) {
		this.modePaiements = modePaiements;
	}
	
	
	
	
	

}
