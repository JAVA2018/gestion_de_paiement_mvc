package com.paiement.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class FluxEntrant implements Serializable {

	@Id
	@GeneratedValue
	private int idFluxEntrant;
	
	private double somme;
	
	@ManyToOne
	@JoinColumn(name = "idCompte")
	private Compte compte;
	
	@ManyToOne
	@JoinColumn(name = "idExercice")
	private Exercice exercice;
	@ManyToOne
	@JoinColumn(name = "idRubrique")
	private Rubrique rubrique;
	

	public int getIdFluxEntrant() {
		return idFluxEntrant;
	}

	public void setIdFluxEntrant(int idFluxEntrant) {
		this.idFluxEntrant = idFluxEntrant;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	public Exercice getExercice() {
		return exercice;
	}

	public void setExercice(Exercice exercice) {
		this.exercice = exercice;
	}
	

}
