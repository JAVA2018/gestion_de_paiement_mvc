package com.paiement.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class OrderPaiement implements Serializable {
	@Id
	@GeneratedValue
	private int idOrderPaiement;
	
	@ManyToOne
	@JoinColumn(name = "idRubrique")
	private Rubrique rubrique;
	@ManyToOne
	@JoinColumn(name = "idModePaiement")
	private Rubrique modepaiement;

	public int getIdOrderPaiement() {
		return idOrderPaiement;
	}

	public void setIdOrderPaiement(int idOrderPaiement) {
		this.idOrderPaiement = idOrderPaiement;
	}
	

}
