package com.paiement.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Etablissement implements Serializable {

	@Id
	@GeneratedValue
	private int idEt;
	private String nomEtablissement;
	
	@OneToMany(mappedBy = "etablissement")
	private List<Compte> comptes;
	@OneToMany(mappedBy = "etablissement")
	private List<Exercice> exercices;
	
	public int getIdEt() {
		return idEt;
	}

	public void setIdEt(int idEt) {
		this.idEt = idEt;
	}

	public String getNomEtablissement() {
		return nomEtablissement;
	}

	public void setNomEtablissement(String nomEtablissement) {
		this.nomEtablissement = nomEtablissement;
	}

	public List<Compte> getComptes() {
		return comptes;
	}

	public void setComptes(List<Compte> comptes) {
		this.comptes = comptes;
	}

	public List<Exercice> getExercices() {
		return exercices;
	}

	public void setExercices(List<Exercice> exercices) {
		this.exercices = exercices;
	}
	
	
}
