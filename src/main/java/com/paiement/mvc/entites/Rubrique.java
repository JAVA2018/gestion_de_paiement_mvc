package com.paiement.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Rubrique implements Serializable {
	@Id
	@GeneratedValue
	private int idRubrique;
	
	private String libelle;
	private String type;
	
	@OneToMany(mappedBy = "rubrique")
	private List<FluxEntrant> fluxEntrants;
	
	@OneToMany(mappedBy = "rubrique")
	private List<Creditouvert> creditouverts;
	@OneToMany(mappedBy = "rubrique")
	private List<OrderPaiement> orderpaiements;

	public int getIdRubrique() {
		return idRubrique;
	}

	public void setIdRubrique(int idRubrique) {
		this.idRubrique = idRubrique;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<FluxEntrant> getFluxEntrants() {
		return fluxEntrants;
	}

	public void setFluxEntrants(List<FluxEntrant> fluxEntrants) {
		this.fluxEntrants = fluxEntrants;
	}

	public List<Creditouvert> getCreditouverts() {
		return creditouverts;
	}

	public void setCreditouverts(List<Creditouvert> creditouverts) {
		this.creditouverts = creditouverts;
	}
	
	

}
