package com.paiement.mvc.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Creditouvert implements Serializable{
	
	@Id
	@GeneratedValue
	private int idCreditouvert;
	
	private double somme;
	@ManyToOne
	@JoinColumn(name = "idCompte")
	private Compte compte;
	
	@ManyToOne
	@JoinColumn(name = "idExercice")
	private Exercice exercice;
	
	@ManyToOne
	@JoinColumn(name = "idRubrique")
	private Rubrique rubrique;
	

	public int getIdCreditouvert() {
		return idCreditouvert;
	}

	public void setIdCreditouvert(int idCreditouvert) {
		this.idCreditouvert = idCreditouvert;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public Compte getCompte() {
		return compte;
	}

	public void setCompte(Compte compte) {
		this.compte = compte;
	}

	public Exercice getExercice() {
		return exercice;
	}

	public void setExercice(Exercice exercice) {
		this.exercice = exercice;
	}

	public Rubrique getRubrique() {
		return rubrique;
	}

	public void setRubrique(Rubrique rubrique) {
		this.rubrique = rubrique;
	}
	
	

}
