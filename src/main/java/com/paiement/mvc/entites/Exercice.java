package com.paiement.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Exercice implements Serializable {

	@Id
	@GeneratedValue
	private int idExercice;
	private int annee;
	@ManyToOne
	@JoinColumn(name = "idEt")
	private Etablissement etablissement;
	@OneToMany(mappedBy = "exercice")
	private List<ModePaiement> modePaiements;
	@OneToMany(mappedBy = "exercice")
	private List<Creditouvert> creditouverts;
	@OneToMany(mappedBy = "exercice")
	private List<FluxEntrant> fluxEntrants;
	

	public int getIdExercice() {
		return idExercice;
	}

	public void setIdExercice(int idExercice) {
		this.idExercice = idExercice;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public Etablissement getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}

	public List<ModePaiement> getModePaiements() {
		return modePaiements;
	}

	public void setModePaiements(List<ModePaiement> modePaiements) {
		this.modePaiements = modePaiements;
	}

	public List<Creditouvert> getCreditouverts() {
		return creditouverts;
	}

	public void setCreditouverts(List<Creditouvert> creditouverts) {
		this.creditouverts = creditouverts;
	}

	public List<FluxEntrant> getFluxEntrants() {
		return fluxEntrants;
	}

	public void setFluxEntrants(List<FluxEntrant> fluxEntrants) {
		this.fluxEntrants = fluxEntrants;
	}
	
	
	

}
